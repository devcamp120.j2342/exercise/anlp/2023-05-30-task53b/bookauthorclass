import models.Author;
import models.Book;

public class App {
    public static void main(String[] args) throws Exception {
        Author author1 = new Author("an","apl@gmail.com",'m');
        Author author2 = new Author("minh","minh@gmail.com",'f');

        System.out.println(author1);
        System.out.println(author2);

        Book book1 = new Book("lang thang", author1, 50000);
        Book book2 = new Book("love", author2, 50000,10);
        System.out.println(book1);
        System.out.println(book2);

    }
}
